SELECT
  id, 
  family_name || ' ' || first_name AS "氏名", 
  college_start_month_year AS "カレッジ開始年月", 
/*  is_end_college AS "カレッジ終了フラグId", */
  CASE is_end_college
    WHEN 0 THEN '在校'
    WHEN 1 THEN '卒業'
    WHEN 2 THEN '休学'
    WHEN 3 THEN 'リタイア'
    ELSE 'その他'
  END AS "カレッジ終了フラグ",
  is_night_college AS "夜間カレッジフラグ" 
FROM
  college_student_info
WHERE
  is_night_college = TRUE
AND
  is_end_college = 0
ORDER BY
  college_start_month_year ASC
;


/* 出席情報テーブル */
SELECT
  *
FROM
  attendance_info
WHERE
  college_student_id = 1
AND
  to_char(attendance_date, 'YYYY-MM-DD') = '2018-02-14'
ORDER BY
  attendance_date ASC
;



SELECT
  csi.id AS "受講生ID",
  csi.family_name || ' ' || csi.first_name AS "受講生名",
  ai.attendance_date AS "出欠席日時",
  ai.is_absence,
  CASE ai.is_absence
    WHEN FALSE THEN '出席'
    WHEN TRUE THEN '欠席'
    ELSE 'その他'
  END AS "欠席フラグ",
  ai.is_contact,
  CASE ai.is_contact
    WHEN FALSE THEN '連絡なし'
    WHEN TRUE THEN '連絡あり'
    ELSE 'その他'
  END AS "連絡フラグ"
FROM
  attendance_info ai
LEFT JOIN
  college_student_info csi
ON
  ai.college_student_id = csi.id
WHERE
  to_char(attendance_date, 'YYYY-MM-DD') = '2018-02-21'
ORDER BY
  csi.id ASC
;
