/* DATA SOURCE */

/*-----------------------------------
m_person_in_charge（担当者マスタテーブル）
------------------------------------*/
INSERT INTO m_person_in_charge (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    regist_date
) VALUES (
    '中舘',-- 姓
    '宏輔',-- 名
    'なかだて',-- 姓かな
    'こうすけ',-- 名かな
    NOW()-- 登録日
);

INSERT INTO m_person_in_charge (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    regist_date
) VALUES (
    '兵藤',
    '慶征',
    'ひょうどう',
    'よしまさ',
    NOW()
);

INSERT INTO m_person_in_charge (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    regist_date
) VALUES (
    '加藤',
    '憲康',
    'かとう',
    'のりやす',
    NOW()
);

INSERT INTO m_person_in_charge (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    regist_date
) VALUES (
    '風見',
    '顕',
    'かざみ',
    'あきら',
    NOW()
);

INSERT INTO m_person_in_charge (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    regist_date
) VALUES (
    '六角',
    '裕昭',
    'ろっかく',
    'ひろあき',
    NOW()
);



/*-----------------------------------
college_student_info（カレッジ受講生情報テーブル）
------------------------------------*/
INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
/*
    gender,
    address,
    incumbent,
    available_start_time,
    number_of_turns,
    hearing_comment,
*/
) VALUES (
    '秋元',-- 姓
    '浩平',-- 名
    'あきもと',-- 姓かな
    'こうへい',-- 名かな
    27,-- 年齢
    2,-- 担当者ID
    '2017-09-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
/*
    1,-- 性別
    '東京都八王子市別所',-- 住所
    '自称プログラマー',-- 現職
    '19:00',-- 参加可能開始時間
    1,-- 転職回数
    '飲食系から転職、コミュ障',-- ヒアリングコメント
*/
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
/*
    gender,
    address,
    incumbent,
    available_start_time,
    number_of_turns,
*/
) VALUES (
    '三橋',-- 姓
    '遼太',-- 名
    'みっつーさん',-- 姓かな
    'りょうちゃん',-- 名かな
    27,-- 年齢
    1,-- 担当者ID
    '2017-10-01',-- カレッジ開始年月
    1,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
/*
    0,-- 性別
    '東京都小金井市中町２丁目１４-１３',-- 住所
    '天才プログラマー',-- 現職
    '20:00',-- 参加可能開始時間
    10,-- 転職回数
*/
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    '高橋',-- 姓
    '湧里',-- 名
    'たかはし',-- 姓かな
    'ゆうり',-- 名かな
    25,-- 年齢
    3,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    2,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    '塩治',-- 姓
    '大樹',-- 名
    'えんな',-- 姓かな
    'だいき',-- 名かな
    22,-- 年齢
    4,-- 担当者ID
    '2018-02-01',-- カレッジ開始年月
    3,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    '風見',-- 姓
    '顕',-- 名
    'かざみ',-- 姓かな
    'あきら',-- 名かな
    31,-- 年齢
    5,-- 担当者ID
    '2017-04-01',-- カレッジ開始年月
    3,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    '満田',-- 姓
    'マンタ',-- 名
    'ミッチーサン',-- 姓かな
    'マンイチ',-- 名かな
    28,-- 年齢
    3,-- 担当者ID
    '2014-10-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト1',-- 姓
    'ユーザー',-- 名
    'テスト1',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト2',-- 姓
    'ユーザー',-- 名
    'テスト2',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト3',-- 姓
    'ユーザー',-- 名
    'テスト3',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト4',-- 姓
    'ユーザー',-- 名
    'テスト4',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト5',-- 姓
    'ユーザー',-- 名
    'テスト5',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト6',-- 姓
    'ユーザー',-- 名
    'テスト6',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト7',-- 姓
    'ユーザー',-- 名
    'テスト7',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト8',-- 姓
    'ユーザー',-- 名
    'テスト8',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト9',-- 姓
    'ユーザー',-- 名
    'テスト9',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト10',-- 姓
    'ユーザー',-- 名
    'テスト10',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト11',-- 姓
    'ユーザー',-- 名
    'テスト11',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト12',-- 姓
    'ユーザー',-- 名
    'テスト12',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト13',-- 姓
    'ユーザー',-- 名
    'テスト13',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト14',-- 姓
    'ユーザー',-- 名
    'テスト14',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト15',-- 姓
    'ユーザー',-- 名
    'テスト15',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト16',-- 姓
    'ユーザー',-- 名
    'テスト16',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト17',-- 姓
    'ユーザー',-- 名
    'テスト17',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト18',-- 姓
    'ユーザー',-- 名
    'テスト18',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト19',-- 姓
    'ユーザー',-- 名
    'テスト19',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト20',-- 姓
    'ユーザー',-- 名
    'テスト20',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト21',-- 姓
    'ユーザー',-- 名
    'テスト21',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト22',-- 姓
    'ユーザー',-- 名
    'テスト22',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト23',-- 姓
    'ユーザー',-- 名
    'テスト23',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト24',-- 姓
    'ユーザー',-- 名
    'テスト24',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト25',-- 姓
    'ユーザー',-- 名
    'テスト25',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト26',-- 姓
    'ユーザー',-- 名
    'テスト26',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト27',-- 姓
    'ユーザー',-- 名
    'テスト27',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト28',-- 姓
    'ユーザー',-- 名
    'テスト28',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト29',-- 姓
    'ユーザー',-- 名
    'テスト29',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト30',-- 姓
    'ユーザー',-- 名
    'テスト30',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト',-- 姓
    'ユーザー',-- 名
    'テスト',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト2',-- 姓
    'ユーザー',-- 名
    'テスト2',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト3',-- 姓
    'ユーザー',-- 名
    'テスト3',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト4',-- 姓
    'ユーザー',-- 名
    'テスト4',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト5',-- 姓
    'ユーザー',-- 名
    'テスト5',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト6',-- 姓
    'ユーザー',-- 名
    'テスト6',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト7',-- 姓
    'ユーザー',-- 名
    'テスト7',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト8',-- 姓
    'ユーザー',-- 名
    'テスト8',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト9',-- 姓
    'ユーザー',-- 名
    'テスト9',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト10',-- 姓
    'ユーザー',-- 名
    'テスト10',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト11',-- 姓
    'ユーザー',-- 名
    'テスト11',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト12',-- 姓
    'ユーザー',-- 名
    'テスト12',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト13',-- 姓
    'ユーザー',-- 名
    'テスト13',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト14',-- 姓
    'ユーザー',-- 名
    'テスト14',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト15',-- 姓
    'ユーザー',-- 名
    'テスト15',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト16',-- 姓
    'ユーザー',-- 名
    'テスト16',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト17',-- 姓
    'ユーザー',-- 名
    'テスト17',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト18',-- 姓
    'ユーザー',-- 名
    'テスト18',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト19',-- 姓
    'ユーザー',-- 名
    'テスト19',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト20',-- 姓
    'ユーザー',-- 名
    'テスト20',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト21',-- 姓
    'ユーザー',-- 名
    'テスト21',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト22',-- 姓
    'ユーザー',-- 名
    'テスト22',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト23',-- 姓
    'ユーザー',-- 名
    'テスト23',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト24',-- 姓
    'ユーザー',-- 名
    'テスト24',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト25',-- 姓
    'ユーザー',-- 名
    'テスト25',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト26',-- 姓
    'ユーザー',-- 名
    'テスト26',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト27',-- 姓
    'ユーザー',-- 名
    'テスト27',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト28',-- 姓
    'ユーザー',-- 名
    'テスト28',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト29',-- 姓
    'ユーザー',-- 名
    'テスト29',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テスト30',-- 姓
    'ユーザー',-- 名
    'テスト30',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-01-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    TRUE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストa',-- 姓
    'ユーザー',-- 名
    'テストa',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-02-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストb',-- 姓
    'ユーザー',-- 名
    'テストb',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2017-10-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストc',-- 姓
    'ユーザー',-- 名
    'テストc',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2017-11-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストd',-- 姓
    'ユーザー',-- 名
    'テストd',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2017-12-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストa',-- 姓
    'ユーザー',-- 名
    'テストa',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2018-02-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストb',-- 姓
    'ユーザー',-- 名
    'テストb',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2017-10-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストc',-- 姓
    'ユーザー',-- 名
    'テストc',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2017-11-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

INSERT INTO college_student_info (
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    age,
    person_in_charge_id,
    college_start_month_year,
    is_end_college,
    is_night_college,
    regist_date
) VALUES (
    'テストd',-- 姓
    'ユーザー',-- 名
    'テストd',-- 姓かな
    'ユーザー',-- 名かな
    30,-- 年齢
    1,-- 担当者ID
    '2017-12-01',-- カレッジ開始年月
    0,-- カレッジ在籍ステータス
    FALSE,-- 夜間フラグ
    NOW()-- 登録日
);

/*-----------------------------------
attendance_info（出欠席情報テーブル）
------------------------------------*/
INSERT INTO attendance_info (
    college_student_id,
    attendance_date,
    is_absence,
    regist_date
) VALUES (
    1,-- 受講生ID
    '2018-02-01 09:00',-- 出席日時
    FALSE,-- 欠席フラグ
    -- 連絡フラグ
    NOW()-- 登録日
);

INSERT INTO attendance_info (
    college_student_id,
    attendance_date,
    is_absence,
    regist_date
) VALUES (
    1,-- 受講生ID
    '2018-02-02 09:00',-- 出席日時
    FALSE,-- 欠席フラグ
    -- 連絡フラグ
    NOW()-- 登録日
);

INSERT INTO attendance_info (
    college_student_id,
    attendance_date,
    is_absence,
    regist_date
) VALUES (
    1,-- 受講生ID
    '2018-02-03 09:00',-- 出席日時
    FALSE,-- 欠席フラグ
    -- 連絡フラグ
    NOW()-- 登録日
);

INSERT INTO attendance_info (
    college_student_id,
    attendance_date,
    is_absence,
    is_contact,
    regist_date
) VALUES (
    1,-- 受講生ID
    '2018-02-04 09:00',-- 出席日時
    TRUE,-- 欠席フラグ
    TRUE,-- 連絡フラグ
    NOW()-- 登録日
);

INSERT INTO attendance_info (
    college_student_id,
    attendance_date,
    is_absence,
    is_contact,
    regist_date
) VALUES (
    1,-- 受講生ID
    '2018-02-05 09:00',-- 出席日時
    TRUE,-- 欠席フラグ
    FALSE,-- 連絡フラグ
    NOW()-- 登録日
);

INSERT INTO attendance_info (
    college_student_id,
    attendance_date,
    is_absence,
    regist_date
) VALUES (
    4,-- 受講生ID
    '2018-02-02 10:00',-- 出席日時
    FALSE,-- 欠席フラグ
    -- 連絡フラグ
    NOW()-- 登録日
);



/*-----------------------------------
administrator_info（管理者情報テーブル）
------------------------------------*/
INSERT INTO administrator_info (
    login_id,
    login_password,
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    authority,
    is_login,
    last_login_time,
    regist_date
) VALUES (
    'internous1',-- ログインID
    'internous01',-- ログインパスワード
    '三橋',-- 姓
    '遼太',-- 名
    'みっつーさん',-- 姓かな
    'りょうちゃん',-- 名かな
    0,-- 権限
    FALSE,-- ログインフラグ
    '2018-01-01 10:00',-- 最終ログイン日時
    NOW()-- 登録日
);

INSERT INTO administrator_info (
    login_id,
    login_password,
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    authority,
    is_login,
    last_login_time,
    regist_date
) VALUES (
    'internous2',-- ログインID
    'internous01',-- ログインパスワード
    '井上',-- 姓
    '琢磨',-- 名
    'いのうえ',-- 姓かな
    'たくさん',-- 名かな
    1,-- 権限
    FALSE,-- ログインフラグ
    '2018-02-01 10:00',-- 最終ログイン日時
    NOW()-- 登録日
);

INSERT INTO administrator_info (
    login_id,
    login_password,
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    authority,
    is_login,
    regist_date
) VALUES (
    'internous3',-- ログインID
    'internous01',-- ログインパスワード
    '兵藤',-- 姓
    '慶征',-- 名
    'ひょうどう',-- 姓かな
    'まささん',-- 名かな
    1,-- 権限
    FALSE,-- ログインフラグ
    -- 最終ログイン日時
    NOW()-- 登録日
);

INSERT INTO administrator_info (
    login_id,
    login_password,
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    authority,
    is_login,
    last_login_time,
    regist_date
) VALUES (
    'internous4',-- ログインID
    'internous01',-- ログインパスワード
    '風見',-- 姓
    '顕',-- 名
    'かざみ',-- 姓かな
    'あきらさん',-- 名かな
    1,-- 権限
    FALSE,-- ログインフラグ
    '2018-02-02 10:01',-- 最終ログイン日時
    NOW()-- 登録日
);

INSERT INTO administrator_info (
    login_id,
    login_password,
    family_name,
    first_name,
    family_name_kana,
    first_name_kana,
    authority,
    is_login,
    last_login_time,
    regist_date
) VALUES (
    'internous5',-- ログインID
    'internous01',-- ログインパスワード
    '荒井',-- 姓
    '桂',-- 名
    'あらい',-- 姓かな
    'けいさん',-- 名かな
    1,-- 権限
    FALSE,-- ログインフラグ
    '2018-02-02 10:02',-- 最終ログイン日時
    NOW()-- 登録日
);



/*-----------------------------------
chime_info（管理者情報テーブル）
------------------------------------*/
INSERT INTO chime_info (
    chime_flg
) VALUES (
    '1'-- チャイムフラグ
);

INSERT INTO chime_info (
    chime_flg
) VALUES (
    '1'-- チャイムフラグ
);

INSERT INTO chime_info (
    chime_flg
) VALUES (
    '1'-- チャイムフラグ
);

INSERT INTO chime_info (
    chime_flg
) VALUES (
    '1'-- チャイムフラグ
);
