TRUNCATE TABLE administrator_info CASCADE;

TRUNCATE TABLE attendance_info CASCADE;

TRUNCATE TABLE chime_info CASCADE;

TRUNCATE TABLE college_student_info CASCADE;

TRUNCATE TABLE m_person_in_charge CASCADE;

TRUNCATE TABLE chime_info CASCADE;

select setval ('administrator_info_id_seq', 1, false);

select setval ('attendance_info_id_seq', 1, false);

select setval ('chime_info_id_seq', 1, false);

select setval ('college_student_info_id_seq', 1, false);

select setval ('m_person_in_charge_id_seq', 1, false);

select setval ('chime_info_id_seq', 1, false);
