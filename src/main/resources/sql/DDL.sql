/* DDL */

/*-----------------------------------
college_absentee_system（カレッジ出欠席システムデータベース）
------------------------------------*/
\c postgres
DROP DATABASE IF EXISTS college_absentee_system;
CREATE DATABASE college_absentee_system;
COMMENT ON DATABASE college_absentee_system IS 'カレッジ出欠席システムデータベース';
\c college_absentee_system



DROP TABLE IF EXISTS chime_info;
DROP TABLE IF EXISTS attendance_info;
DROP TABLE IF EXISTS administrator_info;
DROP TABLE IF EXISTS college_student_info;
DROP TABLE IF EXISTS m_person_in_charge;



/*-----------------------------------
m_person_in_charge（担当者マスタテーブル）
------------------------------------*/
CREATE TABLE m_person_in_charge (
    id                serial PRIMARY KEY,
    family_name       varchar(32) NOT NULL,
    first_name        varchar(32) NOT NULL,
    family_name_kana  varchar(64),
    first_name_kana   varchar(64),
    regist_date       timestamp NOT NULL,
    update_date       timestamp
);
GRANT SELECT, UPDATE, INSERT, TRUNCATE ON m_person_in_charge TO internouslab;
GRANT SELECT, UPDATE ON SEQUENCE m_person_in_charge_id_seq TO internouslab;
COMMENT ON TABLE m_person_in_charge IS '担当者マスタテーブル';
COMMENT ON COLUMN m_person_in_charge.id IS '担当者マスタID';
COMMENT ON COLUMN m_person_in_charge.family_name IS '担当者苗字';
COMMENT ON COLUMN m_person_in_charge.first_name IS '担当者名前';
COMMENT ON COLUMN m_person_in_charge.family_name_kana IS '担当者苗字かな';
COMMENT ON COLUMN m_person_in_charge.first_name_kana IS '担当者名前かな';
COMMENT ON COLUMN m_person_in_charge.regist_date IS '登録日';
COMMENT ON COLUMN m_person_in_charge.update_date IS '更新日';



/*-----------------------------------
m_via（経由マスタテーブル）
------------------------------------*/
CREATE TABLE m_via (
    id           serial PRIMARY KEY,
    via          varchar(64) NOT NULL,
    regist_date  timestamp NOT NULL,
    update_date  timestamp
);
GRANT SELECT, UPDATE, INSERT, TRUNCATE ON m_via TO internouslab;
GRANT SELECT, UPDATE ON SEQUENCE m_via_id_seq TO internouslab;
COMMENT ON TABLE m_via IS '経由マスタテーブル';
COMMENT ON COLUMN m_via.id IS '経由マスタID';
COMMENT ON COLUMN m_via.via IS '経由';
COMMENT ON COLUMN m_via.regist_date IS '登録日';
COMMENT ON COLUMN m_via.update_date IS '更新日';



/*-----------------------------------
college_student_info（カレッジ受講生情報テーブル）
------------------------------------*/
CREATE TABLE college_student_info (
    id                        serial PRIMARY KEY,
    family_name               varchar(32) NOT NULL,
    first_name                varchar(32) NOT NULL,
    family_name_kana          varchar(64) NOT NULL,
    first_name_kana           varchar(64) NOT NULL,
    age                       int,
    enterprise_company        varchar(128) NOT NULL DEFAULT '',
    tel                       varchar(13),
    email                     varchar(128),
    via_id                    int,
    personality               int,
    personality_detail        text,
    git_hub_account           varchar(59),
    team_git_hub_account      varchar(59),
    person_in_charge_id       int,
    college_start_month_year  date NOT NULL,
    is_end_college            int NOT NULL DEFAULT 0,
    is_night_college          boolean NOT NULL,
    regist_date               timestamp NOT NULL,
    update_date               timestamp,
/*
    gender                    int NOT NULL,
    address                   varchar(256),
    incumbent                 varchar(64),
    available_start_time      time,
    number_of_turns           int,
    hearing_comment           text,
*/
    FOREIGN KEY (person_in_charge_id) REFERENCES m_person_in_charge (id),
    FOREIGN KEY (via_id) REFERENCES m_via (id)
);
GRANT SELECT, UPDATE, INSERT, TRUNCATE ON college_student_info TO internouslab;
GRANT SELECT, UPDATE ON SEQUENCE college_student_info_id_seq TO internouslab;
COMMENT ON TABLE college_student_info IS 'カレッジ受講生情報テーブル';
COMMENT ON COLUMN college_student_info.id IS 'カレッジ受講生ID';
COMMENT ON COLUMN college_student_info.family_name IS '受講生苗字';
COMMENT ON COLUMN college_student_info.first_name IS '受講生名前';
COMMENT ON COLUMN college_student_info.family_name_kana IS '受講生苗字かな';
COMMENT ON COLUMN college_student_info.first_name_kana IS '受講生名前かな';
COMMENT ON COLUMN college_student_info.age IS '年齢';
COMMENT ON COLUMN college_student_info.enterprise_company IS '入社企業名';
COMMENT ON COLUMN college_student_info.tel IS '電話番号';
COMMENT ON COLUMN college_student_info.email IS 'メールアドレス';
COMMENT ON COLUMN college_student_info.via_id IS '経由ID';
COMMENT ON COLUMN college_student_info.personality IS '人柄';
COMMENT ON COLUMN college_student_info.personality_detail IS '人柄（詳細）';
COMMENT ON COLUMN college_student_info.git_hub_account IS 'GitHubアカウントURL（個人）';
COMMENT ON COLUMN college_student_info.team_git_hub_account IS 'GitHubアカウントURL（チーム）';
COMMENT ON COLUMN college_student_info.person_in_charge_id IS '担当者ID';
COMMENT ON COLUMN college_student_info.college_start_month_year IS 'カレッジ開始年月';
COMMENT ON COLUMN college_student_info.is_end_college IS 'カレッジ在籍ステータス';
COMMENT ON COLUMN college_student_info.is_night_college IS '夜間フラグ';
COMMENT ON COLUMN college_student_info.regist_date IS '登録日';
COMMENT ON COLUMN college_student_info.update_date IS '更新日';
/*
COMMENT ON COLUMN college_student_info.gender IS '性別';
COMMENT ON COLUMN college_student_info.address IS '住所';
COMMENT ON COLUMN college_student_info.incumbent IS '現職';
COMMENT ON COLUMN college_student_info.available_start_time IS '参加可能開始時間';
COMMENT ON COLUMN college_student_info.number_of_turns IS '転職回数';
COMMENT ON COLUMN college_student_info.hearing_comment IS 'ヒアリングコメント';
*/


/*-----------------------------------
administrator_info（管理者情報テーブル）
------------------------------------*/
CREATE TABLE administrator_info (
    id                serial PRIMARY KEY,
    login_id          varchar(256) NOT NULL UNIQUE,
    login_password    varchar(256) NOT NULL,
    family_name       varchar(32) NOT NULL,
    first_name        varchar(32) NOT NULL,
    family_name_kana  varchar(64) NOT NULL,
    first_name_kana   varchar(64) NOT NULL,
    authority         int NOT NULL DEFAULT 1,
    is_login          boolean NOT NULL DEFAULT FALSE,
    last_login_time   timestamp,
    regist_date       timestamp NOT NULL,
    update_date       timestamp
);
GRANT SELECT, UPDATE, INSERT, TRUNCATE ON administrator_info TO internouslab;
GRANT SELECT, UPDATE ON SEQUENCE administrator_info_id_seq TO internouslab;
COMMENT ON TABLE administrator_info IS '管理者情報テーブル';
COMMENT ON COLUMN administrator_info.id IS '管理者ID';
COMMENT ON COLUMN administrator_info.login_id IS 'ログインID';
COMMENT ON COLUMN administrator_info.login_password IS 'ログインパスワード';
COMMENT ON COLUMN administrator_info.family_name IS '管理者苗字';
COMMENT ON COLUMN administrator_info.first_name IS '管理者名前';
COMMENT ON COLUMN administrator_info.family_name_kana IS '管理者苗字かな';
COMMENT ON COLUMN administrator_info.first_name_kana IS '管理者名前かな';
COMMENT ON COLUMN administrator_info.authority IS '権限';
COMMENT ON COLUMN administrator_info.is_login IS 'ログインフラグ';
COMMENT ON COLUMN administrator_info.last_login_time IS '最終ログイン時間';
COMMENT ON COLUMN administrator_info.regist_date IS '登録日';
COMMENT ON COLUMN administrator_info.update_date IS '更新日';



/*-----------------------------------
attendance_info（出欠席情報テーブル）
------------------------------------*/
CREATE TABLE attendance_info (
    id                  serial PRIMARY KEY,
    college_student_id  int NOT NULL,
    attendance_date     timestamp NOT NULL,
    is_absence          boolean NOT NULL DEFAULT FALSE,
    is_contact          boolean,
    regist_date         timestamp NOT NULL,
    update_date         timestamp,
    FOREIGN KEY (college_student_id) REFERENCES college_student_info (id)
);
GRANT SELECT, UPDATE, INSERT, TRUNCATE ON attendance_info TO internouslab;
GRANT SELECT, UPDATE ON SEQUENCE attendance_info_id_seq TO internouslab;
COMMENT ON TABLE attendance_info IS '出欠席情報テーブル';
COMMENT ON COLUMN attendance_info.id IS '出欠席ID';
COMMENT ON COLUMN attendance_info.college_student_id IS 'カレッジ受講生ID';
COMMENT ON COLUMN attendance_info.attendance_date IS '出席日時';
COMMENT ON COLUMN attendance_info.is_absence IS '欠席フラグ';
COMMENT ON COLUMN attendance_info.is_contact IS '連絡フラグ';
COMMENT ON COLUMN attendance_info.regist_date IS '登録日';
COMMENT ON COLUMN attendance_info.update_date IS '更新日';



/*-----------------------------------
chime_info（チャイム情報テーブル）
------------------------------------*/
CREATE TABLE chime_info (
    id         serial PRIMARY KEY,
    chime_flg  varchar(10) NOT NULL DEFAULT '1'
);
GRANT SELECT, UPDATE, INSERT, TRUNCATE ON chime_info TO internouslab;
GRANT SELECT, UPDATE ON SEQUENCE chime_info_id_seq TO internouslab;
COMMENT ON TABLE chime_info IS 'チャイム情報テーブル';
COMMENT ON COLUMN chime_info.id IS 'チャイムID';
COMMENT ON COLUMN chime_info.chime_flg IS 'チャイムフラグ';





