package jp.co.internouslab.collegeabsenteesystem.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="college_student_info")
@Getter
@Setter
public class CollegeStudentInfoEntity {

	@Id
	@Column(columnDefinition="serial PRIMARY KEY")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="family_name", columnDefinition="varchar(32)", nullable=false)
	private String familyName;

	@Column(name="first_name", columnDefinition="varchar(32)", nullable=false)
	private String firstName;

	@Column(name="family_name_kana", columnDefinition="varchar(64)", nullable=false)
	private String familyNameKana;

	@Column(name="first_name_kana", columnDefinition="varchar(64)", nullable=false)
	private String firstNameKana;

	@Column(name="age", columnDefinition="int")
	private Integer age;

	@Column(name="enterprise_company", columnDefinition="varchar(32)")
	private String enterpriseCompany;

	@Column(name="tel", columnDefinition="varchar(32)")
	private String tel;

	@Column(name="email", columnDefinition="varchar(128)")
	private String email;

	@Column(name="via_id", columnDefinition="int")
	private Integer viaId;

	@Column(name="personality", columnDefinition="int")
	private Integer personality;

	@Column(name="personality_detail", columnDefinition="text")
	private String personalityDetail;

	@Column(name="git_hub_account", columnDefinition="varchar(59)")
	private String gitHubAccount;

	@Column(name="team_git_hub_account", columnDefinition="varchar(59)")
	private String teamGitHubAccount;

	@Column(name="person_in_charge_id", columnDefinition="int")
	private Integer personInChargeId;

	@Column(name="college_start_month_year", columnDefinition="date")
	private Date collegeStartMonthYear;

	@Column(name="is_end_college", columnDefinition="int DEFAULT 0", nullable=false)
	private int isEndCollege;

	@Column(name="is_night_college", columnDefinition="boolean")
	private boolean isNightCollege;

	@Column(name="regist_date", columnDefinition="timestamp", nullable=false)
	private Timestamp registDate;

	@Column(name="update_date", columnDefinition="timestamp")
	private Timestamp updateDate;

}
