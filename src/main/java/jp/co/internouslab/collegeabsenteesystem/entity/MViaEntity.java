package jp.co.internouslab.collegeabsenteesystem.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class MViaEntity {

	@Id
	@Column(columnDefinition="serial PRIMARY KEY")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="via", columnDefinition="varchar(64)", nullable=false)
	private String via;

	@Column(name="regist_date", columnDefinition="timestamp", nullable=false)
	private Timestamp registDate;

	@Column(name="update_date", columnDefinition="timestamp")
	private Timestamp updateDate;

}
