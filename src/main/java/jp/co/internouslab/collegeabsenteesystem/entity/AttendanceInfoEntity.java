package jp.co.internouslab.collegeabsenteesystem.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="attendance_info")
@Getter
@Setter
public class AttendanceInfoEntity {

	@Id
	@Column(columnDefinition="serial PRIMARY KEY")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="college_student_id", columnDefinition="int", nullable=false)
	private int collegeStudentId;

	@Column(name="attendance_date", columnDefinition="timestamp", nullable=false)
	private Timestamp attendanceDate;

	@Column(name="is_absence", columnDefinition="boolean DEFAULT FALSE", nullable=false)
	private Boolean isAbsence;

	@Column(name="is_contact", columnDefinition="boolean")
	private Boolean isContact;

	@Column(name="regist_date", columnDefinition="timestamp", nullable=false)
	private Timestamp registDate;

	@Column(name="update_date", columnDefinition="timestamp")
	private Timestamp updateDate;

}
