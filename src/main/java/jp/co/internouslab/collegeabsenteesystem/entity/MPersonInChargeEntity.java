package jp.co.internouslab.collegeabsenteesystem.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="m_person_in_charge")
@Getter
@Setter
public class MPersonInChargeEntity {

	@Id
	@Column(columnDefinition="serial PRIMARY KEY")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="family_name", columnDefinition="varchar(32)", nullable=false)
	private String familyName;

	@Column(name="first_name", columnDefinition="varchar(32)", nullable=false)
	private String firstName;

	@Column(name="family_name_kana", columnDefinition="varchar(64)")
	private String familyNameKana;

	@Column(name="first_name_kana", columnDefinition="varchar(64)")
	private String firstNameKana;

	@Column(name="regist_date", columnDefinition="timestamp", nullable=false)
	private Timestamp registDate;

	@Column(name="update_date", columnDefinition="timestamp")
	private Timestamp updateDate;

}
