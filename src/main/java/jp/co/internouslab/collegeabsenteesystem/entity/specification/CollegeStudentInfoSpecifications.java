package jp.co.internouslab.collegeabsenteesystem.entity.specification;


import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;

public class CollegeStudentInfoSpecifications {

	public static Specification<CollegeStudentInfoEntity> familyNameContains(String name) {
		return StringUtils.isEmpty(name) ? null : (root, query, cb) -> {
				return cb.like(root.get("familyName"), "%" + name + "%");
		};
	}

	public static Specification<CollegeStudentInfoEntity> firstNameContains(String name) {
		return StringUtils.isEmpty(name) ? null : (root, query, cb) -> {
				return cb.like(root.get("firstName"), "%" + name + "%");
		};
	}

	public static Specification<CollegeStudentInfoEntity> familyNameKanaContains(String name) {
		return StringUtils.isEmpty(name) ? null : (root, query, cb) -> {
				return cb.like(root.get("familyNameKana"), "%" + name + "%");
		};
	}

	public static Specification<CollegeStudentInfoEntity> firstNameKanaContains(String name) {
		return StringUtils.isEmpty(name) ? null : (root, query, cb) -> {
				return cb.like(root.get("firstNameKana"), "%" + name + "%");
		};
	}

	public static Specification<CollegeStudentInfoEntity> enterpriseCompanyContains(String enterpriseCompany) {
		return StringUtils.isEmpty(enterpriseCompany) ? null : (root, query, cb) -> {
				return cb.like(root.get("enterpriseCompany"), "%" + enterpriseCompany + "%");
		};
	}

	public static Specification<CollegeStudentInfoEntity> startYearContains(String startYear) {
		return StringUtils.isEmpty(startYear) ? null : (root, query, cb) -> {
			return cb.equal(cb.function("TO_CHAR", String.class, root.get("collegeStartMonthYear"), cb.literal("YYYY")), startYear);
		};
	}

	public static Specification<CollegeStudentInfoEntity> startMonthContains(String startMonth) {
		return StringUtils.isEmpty(startMonth) ? null : (root, query, cb) -> {
			return cb.equal(cb.function("TO_CHAR", String.class, root.get("collegeStartMonthYear"), cb.literal("MM")), startMonth);
		};
	}

	public static Specification<CollegeStudentInfoEntity> isNightCollegeContains(Boolean isNightCollege) {
		return StringUtils.isEmpty(isNightCollege) ? null : (root, query, cb) -> {
			return cb.equal(root.get("isNightCollege"), isNightCollege);
		};
	}

}
