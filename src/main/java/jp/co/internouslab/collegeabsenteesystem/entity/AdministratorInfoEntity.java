package jp.co.internouslab.collegeabsenteesystem.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="administrator_info")
@Getter
@Setter
public class AdministratorInfoEntity {

	@Id
	@Column(columnDefinition="serial PRIMARY KEY")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="login_id", columnDefinition="varchar(256) UNIQUE", nullable=false)
	private String loginId;

	@Column(name="login_password", columnDefinition="varchar(256)", nullable=false)
	private String loginPassword;

	@Column(name="family_name", columnDefinition="varchar(32)", nullable=false)
	private String familyName;

	@Column(name="first_name", columnDefinition="varchar(32)", nullable=false)
	private String firstName;

	@Column(name="family_name_kana", columnDefinition="varchar(64)", nullable=false)
	private String familyNameKana;

	@Column(name="first_name_kana", columnDefinition="varchar(64)", nullable=false)
	private String firstNameKana;

	@Column(name="authority", columnDefinition="int DEFAULT 1", nullable=false)
	private int authority;

	@Column(name="is_login", columnDefinition="boolean DEFAULT FALSE", nullable=false)
	private boolean isLogin;

	@Column(name="last_login_time", columnDefinition="timestamp")
	private Timestamp lastLoginTime;

	@Column(name="regist_date", columnDefinition="timestamp", nullable=false)
	private Timestamp registDate;

	@Column(name="update_date", columnDefinition="timestamp")
	private Timestamp updateDate;

}
