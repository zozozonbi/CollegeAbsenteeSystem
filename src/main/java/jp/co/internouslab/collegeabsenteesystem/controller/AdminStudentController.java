package jp.co.internouslab.collegeabsenteesystem.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;

import jp.co.internouslab.collegeabsenteesystem.service.GetDetailStudentServiceInterface;
import jp.co.internouslab.collegeabsenteesystem.service.GetStudentsServiceInterface;
import jp.co.internouslab.collegeabsenteesystem.service.PostStudentServiceInterface;
import jp.co.internouslab.collegeabsenteesystem.service.PutStudentServiceInterface;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jp.co.internouslab.collegeabsenteesystem.form.PostStudentForm;
import jp.co.internouslab.collegeabsenteesystem.form.PutStudentForm;
import lombok.Getter;
import lombok.Setter;

//@CrossOrigin(origins="http://localhost:4200")
@RestController
@CrossOrigin
@RequestMapping("/student")
@Getter
@Setter
public class AdminStudentController {

	@Autowired
	private PostStudentServiceInterface postStudentService;

	@Autowired
	private PutStudentServiceInterface putStudentService;

	@Autowired
	private GetStudentsServiceInterface getStudentsService;

	@Autowired
	private GetDetailStudentServiceInterface getDetailStudentService;

	// http://localhost:8080/student/postStudent?familyName=Akimoto&firstName=Kohei&familyNameKana=アキモト&firstNameKana=コウヘイ&startYear=2018&startMonth=4&isNightCollege=0
	@PostMapping("/postStudent")
	private Map<String, Object> postStudent(@ModelAttribute @Validated PostStudentForm request) {
		return postStudentService.postStudent(request);
	}

	// http://localhost:8080/student/putStudent?id=76&familyName=akimoto&firstName=浩平&familyNameKana=アキモト&firstNameKana=コウヘイ&age=19&enterpriseCompany=&tel=""&email=""&viaId=&personality=&personalityDetail=""&gitHubAccount=""&teamGitHubAccount=""&personInChargeId=&startYear=2018&startMonth=8&isEndCollege=0&isNightCollege=0&registDate=2018-02-21 15:06:00
	@PutMapping("/putStudent")
	private Map<String, Object> putStudent(@ModelAttribute @Validated PutStudentForm request) {
		return putStudentService.putStudent(request);
	}

	// http://localhost:8080/student/getStudents?name=&enterpriseCompany=&startYear=2014&startMonth=&isNightCollege=1
	@GetMapping("/getStudents")
	private Map<String, Object> getStudents(@RequestParam(name = "name", defaultValue = "") String name, @RequestParam(name = "enterpriseCompany", defaultValue = "") String enterpriseCompany, @RequestParam(name = "startYear", defaultValue = "") String startYear, @RequestParam(name = "startMonth", defaultValue = "") String startMonth, @RequestParam(name = "isNightCollege", defaultValue = "") Boolean isNightCollege) {
		return getStudentsService.getStudents(name, enterpriseCompany, startYear, startMonth, isNightCollege);
	}

	// http://localhost:8080/student/getDetailStudent/6
	@GetMapping("/getDetailStudent/{studentId}")
	private Map<String, Object> getDetailStudent(@PathVariable("studentId") int studentId) {
		return getDetailStudentService.getDetailStudent(studentId);
	}

}
