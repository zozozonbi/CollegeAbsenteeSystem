package jp.co.internouslab.collegeabsenteesystem.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import jp.co.internouslab.collegeabsenteesystem.form.PostAttendanceForm;
import jp.co.internouslab.collegeabsenteesystem.service.GetMonthServiceInterface;
import jp.co.internouslab.collegeabsenteesystem.service.GetStudentsNameServiceInterface;
import jp.co.internouslab.collegeabsenteesystem.service.PostAttendanceServiceInterface;
import lombok.Getter;
import lombok.Setter;


//@CrossOrigin(origins="http://localhost:4200")
@RestController
@CrossOrigin(origins="http://192.168.1.99:4200")
@RequestMapping("/attendance")
@Getter
@Setter
public class UserAttendanceController {

	@Autowired
	private GetMonthServiceInterface getMonthService;

	@Autowired
	private GetStudentsNameServiceInterface getStudentsNameService;

	@Autowired
	private PostAttendanceServiceInterface postAttendanceService;


	// http://localhost:8080/attendance/getMonth/0
	@GetMapping(value="/getMonth/{isNightCollege}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Map<String, Object> getMonth(@PathVariable boolean isNightCollege) {

		return getMonthService.getMonth(isNightCollege);

	}

	// http://localhost:8080/attendance/getStudent/2018-05-01　00:00:00/1
	@GetMapping(value="/getStudents/{startMonthYear}/{isNightCollege}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Map<String, Object> getStudents(@PathVariable("startMonthYear") String startMonthYear, @PathVariable("isNightCollege") boolean isNightCollege) {

		return getStudentsNameService.getStudentsName(startMonthYear, isNightCollege);

	}

	// http://localhost:8080/attendance/postAttendance?isAbsence=1&collegeStudentId=2&isContact=0
	@PostMapping(value="/postAttendance", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Map<String, String> postAttendance(@ModelAttribute @Validated PostAttendanceForm request) {

		if (request.getCollegeStudentId() == null) {
			Map<String, String> res = new HashMap<>();
			res.put("status", "400");
			res.put("message", "BAD REQUEST");
			return res;
		}

		return postAttendanceService.postAttendance(request);
	}

}
