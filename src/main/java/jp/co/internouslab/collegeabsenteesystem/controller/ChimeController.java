package jp.co.internouslab.collegeabsenteesystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jp.co.internouslab.collegeabsenteesystem.entity.TChimeEntity;
import jp.co.internouslab.collegeabsenteesystem.form.ChimeForm;
import jp.co.internouslab.collegeabsenteesystem.service.ChimeServiceInterface;

@RestController
//@CrossOrigin(origins="http://localhost:4200")
//@CrossOrigin(origins="http://ipv4.fiddler:4200")
@CrossOrigin(origins="http://192.168.1.99:4200")
@RequestMapping("/")
public class ChimeController {

	@Autowired
	private ChimeServiceInterface chimeServiceImpl;

	@GetMapping("/chime")
	public List<TChimeEntity> checkChimeFlg() {
		return chimeServiceImpl.chekStartChimeFlg();
	}

	@RequestMapping(value= "/chime/start", method = RequestMethod.POST)
	public boolean startChime(@ModelAttribute ChimeForm chimeFrom) {
		chimeServiceImpl.updateStopChimeFlg(chimeFrom);
		return true;
	}

	@RequestMapping(value= "/chime/stop", method = RequestMethod.POST)
	public boolean stopChime(@ModelAttribute ChimeForm chimeFrom) {
		chimeServiceImpl.stopChimeFlg(chimeFrom);
		return true;
	}
}
