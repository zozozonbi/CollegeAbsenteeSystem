package jp.co.internouslab.collegeabsenteesystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;


@Repository
@Transactional
public interface CollegeStudentInfoRepository extends JpaRepository<CollegeStudentInfoEntity, Integer>, JpaSpecificationExecutor<CollegeStudentInfoEntity> {

	public List<CollegeStudentInfoEntity> findByIsNightCollegeAndIsEndCollegeOrderByCollegeStartMonthYearAsc(boolean isNigthtCollege, int isEndCollege) throws Exception;

	public List<CollegeStudentInfoEntity> findByCollegeStartMonthYearAndIsNightCollegeAndIsEndCollegeOrderByCollegeStartMonthYearAsc(Date collegeStartMonthYear, boolean isNigthtCollege, int isEndCollege) throws Exception;

	public List<CollegeStudentInfoEntity> findById(int studentId) throws Exception;

}
