package jp.co.internouslab.collegeabsenteesystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.internouslab.collegeabsenteesystem.entity.AdministratorInfoEntity;

public interface AdministratorInfoRepository extends JpaRepository<AdministratorInfoEntity, Integer> {

	

}
