package jp.co.internouslab.collegeabsenteesystem.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import jp.co.internouslab.collegeabsenteesystem.entity.AttendanceInfoEntity;

@Repository
@Transactional
public interface AttendanceInfoRepository extends JpaRepository<AttendanceInfoEntity, Integer> {

	@Query(value = "SELECT "
			+ "count(*) "
			+ "FROM "
			+ "attendance_info "
			+ "WHERE "
			+ "college_student_id = ? "
			+ "AND "
			+ "to_char(attendance_date, 'YYYY-MM-DD') = ? ",
			nativeQuery = true)
	public int findCountByIdAndAttendanceDate(int id, String attendanceDate) throws Exception;

	@Query(value = "UPDATE "
			+ "attendance_info "
			+ "SET "
			+ "is_absence = ? "
			+ "WHERE "
			+ "id = ? ",
			nativeQuery = true)
	@Modifying
	public int updateIsAbsence(boolean isAbsence, int id) throws Exception;

}
