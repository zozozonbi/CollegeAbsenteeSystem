package jp.co.internouslab.collegeabsenteesystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.internouslab.collegeabsenteesystem.entity.MPersonInChargeEntity;

public interface MPersonInChargeRepository extends JpaRepository<MPersonInChargeEntity, Integer> {

}
