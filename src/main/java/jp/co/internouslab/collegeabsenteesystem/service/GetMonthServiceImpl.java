package jp.co.internouslab.collegeabsenteesystem.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;
import jp.co.internouslab.collegeabsenteesystem.repository.CollegeStudentInfoRepository;
import jp.co.internouslab.collegeabsenteesystem.response.GetMonthResponse;

@Service
public class GetMonthServiceImpl implements GetMonthServiceInterface {

	@Autowired
	private CollegeStudentInfoRepository collegeStudentInfoRepository;

	@Override
	public Map<String, Object> getMonth(boolean isNightCollege) {

		Map<String, Object> res = new HashMap<>();

		try {

			List<CollegeStudentInfoEntity> findRes = collegeStudentInfoRepository.findByIsNightCollegeAndIsEndCollegeOrderByCollegeStartMonthYearAsc(isNightCollege, 0);

			if (findRes.size() == 0) {
				res.put("status", "204");
				res.put("message", "NO CONTENT");
				return res;
			}

			List<GetMonthResponse> monthList = new ArrayList<>();

			for (int i=0; i < findRes.size(); i++) {
				Date collegeStartMonthYear = findRes.get(i).getCollegeStartMonthYear();

				if (i == 0 || !findRes.get(i-1).getCollegeStartMonthYear().toString().equals(collegeStartMonthYear.toString())) {
					monthList.add(GetMonthResponse
							.builder()
							.month(String.format("%02d", LocalDateTime.ofInstant(collegeStartMonthYear.toInstant(), ZoneId.systemDefault()).getMonthValue()))
							.startMonthYear(collegeStartMonthYear.toString())
							.build());
				}

			}

			res.put("status", "200");
			res.put("message", "OK");
			res.put("monthList", monthList);
			res.put("isNightCollege", isNightCollege);

		} catch(Exception e) {
			e.printStackTrace();
			res.put("status", "500");
			res.put("message", "INTERNAL SERVER ERROR");

		}

		return res;

	}

}
