package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.TChimeEntity;
import jp.co.internouslab.collegeabsenteesystem.form.ChimeForm;
import jp.co.internouslab.collegeabsenteesystem.repository.TChimeRepository;

@Service
public class ChimeServiceImpl implements ChimeServiceInterface{

	@Autowired
	private TChimeRepository tChimeRepository;

	@Override
	public void updateStopChimeFlg(ChimeForm chimeFrom) {
		tChimeRepository.saveByStopChimeFlg(chimeFrom.isStopChimeFlg(), chimeFrom.getId());
	}

	@Override
	public List<TChimeEntity> chekStartChimeFlg() {
		List<TChimeEntity> tChimeEntityList = tChimeRepository.findAll();
		return tChimeEntityList;
	}

	@Override
	public void stopChimeFlg(ChimeForm chimeFrom) {
		tChimeRepository.saveAllStopChimeFlg(chimeFrom.isStopChimeFlg());
	}

}
