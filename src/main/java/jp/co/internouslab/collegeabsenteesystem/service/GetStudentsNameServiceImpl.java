package jp.co.internouslab.collegeabsenteesystem.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;
import jp.co.internouslab.collegeabsenteesystem.repository.AttendanceInfoRepository;
import jp.co.internouslab.collegeabsenteesystem.repository.CollegeStudentInfoRepository;
import jp.co.internouslab.collegeabsenteesystem.response.GetStudentsNameResponce;




@Service
public class GetStudentsNameServiceImpl implements GetStudentsNameServiceInterface {

	@Autowired
	CollegeStudentInfoRepository collegeStudentInfoRepository;

	@Autowired
	AttendanceInfoRepository attendanceInfoRepository;

	@Override
	public Map<String, Object> getStudentsName(String startMonthYear, boolean isNightCollege) {
		Map<String, Object> res = new HashMap<>();
		try {
			DateFormat startMonthYearFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date dateStartMonthYear = startMonthYearFormat.parse(startMonthYear.replace("-", "/"));
		    List<CollegeStudentInfoEntity> findRes = 
		    		collegeStudentInfoRepository.findByCollegeStartMonthYearAndIsNightCollegeAndIsEndCollegeOrderByCollegeStartMonthYearAsc(
		    				dateStartMonthYear, 
		    				isNightCollege, 
		    				0
		    				);

		    if (findRes.size() == 0) {
				res.put("status", "204");
				res.put("message", "NO CONTENT");
				return res;
			}

		    List<GetStudentsNameResponce> studentList = new ArrayList<>();
		    String nowLocalDate = LocalDate.now().toString();

		    for (int i = 0; findRes.size() > i; i++) {
		    	boolean isAttendance = false;
		    	if (attendanceInfoRepository.findCountByIdAndAttendanceDate(findRes.get(i).getId(), nowLocalDate) > 0) {
		    		isAttendance = true;
		    	};
		    	studentList.add(GetStudentsNameResponce
		    			.builder()
		    			.id(findRes.get(i).getId())
		    			.studentName(findRes.get(i).getFamilyName() + " " + findRes.get(i).getFirstName())
		    			.isAttendance(isAttendance)
		    			.build());
		    }

		    res.put("status", "200");
		    res.put("message", "OK");
		    res.put("studentList", studentList);

		} catch(Exception e) {
			e.printStackTrace();
			res.put("status", "500");
			res.put("message", "INTERNAL SERVER ERROR");
		}
		return res;
	}

}
