package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;
import static jp.co.internouslab.collegeabsenteesystem.entity.specification.CollegeStudentInfoSpecifications.*;
import jp.co.internouslab.collegeabsenteesystem.repository.CollegeStudentInfoRepository;
import jp.co.internouslab.collegeabsenteesystem.response.GetStudentsNameResponce;

@Service
public class GetStudentsServiceImpl implements GetStudentsServiceInterface {

	@Autowired
	private CollegeStudentInfoRepository collegeStudentInfoRepository;

	@Override
	public Map<String, Object> getStudents(String name, String enterpriseCompany, String startYear, String startMonth, Boolean isNightCollege) {
		Map<String, Object> res = new HashMap<>();

		try {
			List<CollegeStudentInfoEntity> findRes = collegeStudentInfoRepository.findAll(Specifications
					.where(familyNameContains(name))
					.or(firstNameContains(name))
					.or(familyNameKanaContains(name))
					.or(firstNameKanaContains(name))
					.and(enterpriseCompanyContains(enterpriseCompany))
					.and(startYearContains(startYear))
					.and(startMonthContains(startMonth))
					.and(isNightCollegeContains(isNightCollege))
					);

			if (findRes.size() == 0) {
				res.put("status", "204");
				res.put("message", "NO CONTENT");
				return res;
			}

			List<GetStudentsNameResponce> studentList = new ArrayList<>();

			for (int i = 0; findRes.size() > i; i++) {
				studentList.add(GetStudentsNameResponce
						.builder()
						.id(findRes.get(i).getId())
						.studentName(findRes.get(i).getFamilyName() + " " + findRes.get(i).getFirstName())
						.build());
			}

			res.put("status", "200");
			res.put("message", "OK");
			res.put("studentList", studentList);

		} catch(Exception e) {
			e.printStackTrace();
			res.put("status", "500");
			res.put("message", "INTERNAL SERVER ERROR");
		}

		return res;
	}

}
