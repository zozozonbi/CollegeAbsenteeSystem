package jp.co.internouslab.collegeabsenteesystem.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;
import jp.co.internouslab.collegeabsenteesystem.entity.MPersonInChargeEntity;
import jp.co.internouslab.collegeabsenteesystem.repository.CollegeStudentInfoRepository;
import jp.co.internouslab.collegeabsenteesystem.repository.MPersonInChargeRepository;
import jp.co.internouslab.collegeabsenteesystem.response.GetDetailStudentResponse;
import jp.co.internouslab.collegeabsenteesystem.response.MPersonInChargeResponse;
import static jp.co.internouslab.collegeabsenteesystem.util.TimestampToString.*;

@Service
public class GetDetailStudentServiceImpl implements GetDetailStudentServiceInterface {

	@Autowired
	CollegeStudentInfoRepository collegeStudentInfoRepository;

	@Autowired
	MPersonInChargeRepository mPersonInChargeRepository;

	@Override
	public Map<String, Object> getDetailStudent(int studentId) {
		Map<String, Object> res = new HashMap<>();

		try {

			List<CollegeStudentInfoEntity> findRes = collegeStudentInfoRepository.findById(studentId);

			if (findRes.size() == 0) {
				res.put("status", "204");
				res.put("message", "NO CONTENT");
				return res;
			} else if(findRes.size() > 1) {
				res.put("status", "203");
				res.put("message", "MANY CONTENTS");
				return res;
			}

			List<MPersonInChargeEntity> mPersonInChargeRes = mPersonInChargeRepository.findAll();
			List<MPersonInChargeResponse> mPersonInChargeList = new ArrayList<>();
			for (int i = 0; mPersonInChargeRes.size() > i; i++) {
				mPersonInChargeList.add(MPersonInChargeResponse
						.builder()
						.id(mPersonInChargeRes.get(i).getId())
						.name(mPersonInChargeRes.get(i).getFamilyName() + " " + mPersonInChargeRes.get(i).getFirstName())
						.build());
			}

			Date collegeStartMonthYear = findRes.get(0).getCollegeStartMonthYear();

			res.put("status", "200");
		    res.put("message", "OK");
		    res.put("studentDetailList", GetDetailStudentResponse
		    		.builder()
		    		.id(findRes.get(0).getId())
	    			.familyName(findRes.get(0).getFamilyName())
	    			.firstName(findRes.get(0).getFirstName())
	    			.familyNameKana(findRes.get(0).getFamilyNameKana())
	    			.firstNameKana(findRes.get(0).getFirstNameKana())
	    			.age(findRes.get(0).getAge())
	    			.enterpriseCompany(findRes.get(0).getEnterpriseCompany())
	    			.tel(findRes.get(0).getTel())
	    			.email(findRes.get(0).getEmail())
	    			.viaId(findRes.get(0).getViaId())
	    			.personality(findRes.get(0).getPersonality())
	    			.personalityDetail(findRes.get(0).getPersonalityDetail())
	    			.gitHubAccount(findRes.get(0).getGitHubAccount())
	    			.teamGitHubAccount(findRes.get(0).getTeamGitHubAccount())
	    			.personInChargeId(findRes.get(0).getPersonInChargeId())
	    			.startYear(String.valueOf(LocalDateTime.ofInstant(collegeStartMonthYear.toInstant(), ZoneId.systemDefault()).getYear()))
	    			.startMonth(String.format("%02d", LocalDateTime.ofInstant(collegeStartMonthYear.toInstant(), ZoneId.systemDefault()).getMonthValue()))
	    			.isEndCollege(findRes.get(0).getIsEndCollege())
	    			.isNightCollege(findRes.get(0).isNightCollege())
	    			.registDate(checkDate(findRes.get(0).getRegistDate()))
	    			.updateDate(checkDate(findRes.get(0).getUpdateDate()))
		    		.build());
		    res.put("mPersonInChargeList", mPersonInChargeList);

		} catch(Exception e) {
			e.printStackTrace();
			res.put("status", "500");
			res.put("message", "INTERNAL SERVER ERROR");
		}
		return res;
	}

}
