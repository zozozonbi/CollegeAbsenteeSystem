package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Map;

import jp.co.internouslab.collegeabsenteesystem.form.PostAttendanceForm;

public interface PostAttendanceServiceInterface {

	public Map<String, String> postAttendance(PostAttendanceForm request);

}
