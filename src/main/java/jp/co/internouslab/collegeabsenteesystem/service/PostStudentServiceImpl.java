package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;
import jp.co.internouslab.collegeabsenteesystem.form.PostStudentForm;
import jp.co.internouslab.collegeabsenteesystem.repository.CollegeStudentInfoRepository;

@Service
public class PostStudentServiceImpl implements PostStudentServiceInterface {

	@Autowired
	private CollegeStudentInfoRepository collegeStudentInfoRepository;

	@Override
	@Transactional
	public Map<String, Object> postStudent(PostStudentForm request) {
		Map<String, Object> res = new HashMap<>();

		try {

			String startYear = request.getStartYear();
			String startMonth = request.getStartMonth();

			if (startYear == null || startYear == "") {
				startYear = String.valueOf(LocalDate.now().getYear());
			}

			if (startMonth == null || startMonth == "") {
				startMonth = String.valueOf(LocalDate.now().getMonthValue());
			}

			String startMonthYear = startYear + "-" + startMonth + "-" + "01";
			DateFormat startMonthYearFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dateStartMonthYear = startMonthYearFormat.parse(startMonthYear);

			CollegeStudentInfoEntity collegeStudentInfoEntity = new CollegeStudentInfoEntity();
			collegeStudentInfoEntity.setFamilyName(request.getFamilyName());
			collegeStudentInfoEntity.setFirstName(request.getFirstName());
			collegeStudentInfoEntity.setFamilyNameKana(request.getFamilyNameKana());
			collegeStudentInfoEntity.setFirstNameKana(request.getFirstNameKana());
			collegeStudentInfoEntity.setAge(request.getAge());
			collegeStudentInfoEntity.setEnterpriseCompany(request.getEnterpriseCompany());
			collegeStudentInfoEntity.setTel(request.getTel());
			collegeStudentInfoEntity.setEmail(request.getEmail());
			collegeStudentInfoEntity.setViaId(request.getViaId());
			collegeStudentInfoEntity.setPersonality(request.getPersonality());
			collegeStudentInfoEntity.setPersonalityDetail(request.getPersonalityDetail());
			collegeStudentInfoEntity.setGitHubAccount(request.getGitHubAccount());
			collegeStudentInfoEntity.setTeamGitHubAccount(request.getTeamGitHubAccount());
			collegeStudentInfoEntity.setPersonInChargeId(request.getPersonInChargeId());
			collegeStudentInfoEntity.setCollegeStartMonthYear(dateStartMonthYear);
			collegeStudentInfoEntity.setIsEndCollege(request.getIsEndCollege());
			collegeStudentInfoEntity.setNightCollege(request.getIsNightCollege());
			collegeStudentInfoEntity.setRegistDate(new Timestamp(new Date().getTime()));

			collegeStudentInfoRepository.save(collegeStudentInfoEntity);

			res.put("status", "200");
			res.put("message", "OK");
			res.put("insertCount", "1");

		} catch(Exception e) {
			e.printStackTrace();
			res.put("status", "500");
			res.put("message", "INTERNAL SERVER ERROR");
		}

		return res;
	}

}
