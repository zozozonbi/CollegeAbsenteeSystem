package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Map;

public interface GetStudentsServiceInterface {

	public Map<String, Object> getStudents(String name, String enterpriseCompany, String startYear, String startMonth, Boolean isNightCollege);

}
