package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Map;

public interface GetMonthServiceInterface {

	public Map<String, Object> getMonth(boolean isNightCollege);

}
