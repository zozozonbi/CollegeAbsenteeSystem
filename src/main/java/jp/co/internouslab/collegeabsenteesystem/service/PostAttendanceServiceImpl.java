package jp.co.internouslab.collegeabsenteesystem.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.AttendanceInfoEntity;
import jp.co.internouslab.collegeabsenteesystem.form.PostAttendanceForm;
import jp.co.internouslab.collegeabsenteesystem.repository.AttendanceInfoRepository;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class PostAttendanceServiceImpl implements PostAttendanceServiceInterface {

	@Autowired
	private AttendanceInfoRepository attendanceInfoRepository;

	@Override
	public Map<String, String> postAttendance(PostAttendanceForm request) {
		Map<String, String> res = new HashMap<>();
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date attendanceDate = dateFormat.parse(request.getAttendanceDate().replace("-", "/"));
			Timestamp attendanceTimeStamp = new Timestamp(attendanceDate.getTime());

			AttendanceInfoEntity attendanceInfoEntity = new AttendanceInfoEntity();
			attendanceInfoEntity.setCollegeStudentId(Integer.parseInt(request.getCollegeStudentId()));
			attendanceInfoEntity.setAttendanceDate(attendanceTimeStamp);
			attendanceInfoEntity.setIsAbsence(request.getIsAbsence());
			attendanceInfoEntity.setIsContact(request.getIsContact());
			attendanceInfoEntity.setRegistDate(new Timestamp(new Date().getTime()));

			attendanceInfoRepository.save(attendanceInfoEntity);

			res.put("status", "200");
			res.put("message", "OK");
			res.put("insertCount", "1");

		} catch(Exception e) {
			e.printStackTrace();
			res.put("status", "500");
			res.put("message", "INTERNAL SERVER ERROR");
		}
		return res;
	}

}
