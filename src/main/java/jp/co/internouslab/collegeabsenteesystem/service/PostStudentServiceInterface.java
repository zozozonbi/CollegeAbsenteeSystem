package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Map;

import jp.co.internouslab.collegeabsenteesystem.form.PostStudentForm;

public interface PostStudentServiceInterface {

	public Map<String, Object> postStudent(PostStudentForm request);

}
