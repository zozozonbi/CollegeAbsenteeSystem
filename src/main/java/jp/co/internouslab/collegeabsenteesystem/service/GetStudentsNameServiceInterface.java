package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Map;

public interface GetStudentsNameServiceInterface {

	public Map<String, Object> getStudentsName(String startMonthYear, boolean isNightCollege);

}
