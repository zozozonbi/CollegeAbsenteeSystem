package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Map;

public interface GetDetailStudentServiceInterface {

	public Map<String, Object> getDetailStudent(int studentId);

}
