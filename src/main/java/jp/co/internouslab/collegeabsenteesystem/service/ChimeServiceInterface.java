package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.List;

import jp.co.internouslab.collegeabsenteesystem.entity.TChimeEntity;
import jp.co.internouslab.collegeabsenteesystem.form.ChimeForm;

public interface ChimeServiceInterface {

	public void updateStopChimeFlg(ChimeForm chimeFrom);

	public List<TChimeEntity> chekStartChimeFlg();

	public void stopChimeFlg(ChimeForm chimeFrom);
}
