package jp.co.internouslab.collegeabsenteesystem.service;

import java.util.Map;

import jp.co.internouslab.collegeabsenteesystem.form.PutStudentForm;

public interface PutStudentServiceInterface {

	public Map<String, Object> putStudent(PutStudentForm request);

}
