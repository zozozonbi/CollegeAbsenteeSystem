package jp.co.internouslab.collegeabsenteesystem.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.internouslab.collegeabsenteesystem.entity.CollegeStudentInfoEntity;
import jp.co.internouslab.collegeabsenteesystem.form.PutStudentForm;
import jp.co.internouslab.collegeabsenteesystem.repository.CollegeStudentInfoRepository;

@Service
public class PutStudentServiceImpl implements PutStudentServiceInterface {

	@Autowired
	private CollegeStudentInfoRepository collegeStudentInfoRepository;

	@Override
	public Map<String, Object> putStudent(PutStudentForm request) {
		Map<String, Object> res = new HashMap<>();

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String startYearMonth = request.getStartYear() + "-" + request.getStartMonth() + "-01";
			Date dateStartYearMonth = sdf.parse(startYearMonth);

			CollegeStudentInfoEntity collegeStudentInfoEntity = new CollegeStudentInfoEntity();
			collegeStudentInfoEntity.setId(request.getId());
			collegeStudentInfoEntity.setFamilyName(request.getFamilyName());
			collegeStudentInfoEntity.setFirstName(request.getFirstName()); 
			collegeStudentInfoEntity.setFamilyNameKana(request.getFamilyNameKana());
			collegeStudentInfoEntity.setFirstNameKana(request.getFirstNameKana());
			collegeStudentInfoEntity.setAge(request.getAge());
			collegeStudentInfoEntity.setEnterpriseCompany(request.getEnterpriseCompany());
			collegeStudentInfoEntity.setTel(request.getTel());
			collegeStudentInfoEntity.setEmail(request.getEmail());
			collegeStudentInfoEntity.setViaId(request.getViaId());
			collegeStudentInfoEntity.setPersonality(request.getPersonality());
			collegeStudentInfoEntity.setPersonalityDetail(request.getPersonalityDetail());
			collegeStudentInfoEntity.setGitHubAccount(request.getGitHubAccount());
			collegeStudentInfoEntity.setTeamGitHubAccount(request.getTeamGitHubAccount());
			collegeStudentInfoEntity.setPersonInChargeId(request.getPersonInChargeId());
			collegeStudentInfoEntity.setCollegeStartMonthYear(dateStartYearMonth);
			collegeStudentInfoEntity.setIsEndCollege(request.getIsEndCollege());
			collegeStudentInfoEntity.setNightCollege(request.isNightCollege());
			collegeStudentInfoEntity.setRegistDate(request.getRegistDate());
			collegeStudentInfoEntity.setUpdateDate(new Timestamp(new Date().getTime()));
			collegeStudentInfoRepository.save(collegeStudentInfoEntity);

			res.put("status", "200");
			res.put("message", "OK");
			res.put("updateCount", 1);

		} catch(Exception e) {
			e.printStackTrace();
			res.put("status", "500");
			res.put("message", "INTERNAL SERVER ERROR");
		}

		return res;
	}

}
