package jp.co.internouslab.collegeabsenteesystem.util;

import java.sql.Timestamp;

public class TimestampToString {

	public static String checkDate(Timestamp timestamp) {
		String strTimestamp = "";

		if (timestamp == null) {
			strTimestamp = null;
		} else {
			strTimestamp = timestamp.toString();
		}

		return strTimestamp;
	}

}
