package jp.co.internouslab.collegeabsenteesystem.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class GetMonthResponse {

	private String month;

	private String startMonthYear;

}
