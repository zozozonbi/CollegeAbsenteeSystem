package jp.co.internouslab.collegeabsenteesystem.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class GetStudentsNameResponce {

	private int id;

	private String studentName;

	private boolean isAttendance;

}
