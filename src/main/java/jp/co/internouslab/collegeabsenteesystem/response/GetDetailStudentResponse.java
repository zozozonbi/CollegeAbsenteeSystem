package jp.co.internouslab.collegeabsenteesystem.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class GetDetailStudentResponse {

	private int id;

	private String familyName;

	private String firstName;

	private String familyNameKana;

	private String firstNameKana;

	private Integer age;

	private String enterpriseCompany;

	private String tel;

	private String email;

	private Integer viaId;

	private Integer personality;

	private String personalityDetail;

	private String gitHubAccount;

	private String teamGitHubAccount;

	private Integer personInChargeId;

	private String startYear;

	private String startMonth;

	private int isEndCollege;

	private boolean isNightCollege;

	private String registDate;

	private String updateDate;

}
