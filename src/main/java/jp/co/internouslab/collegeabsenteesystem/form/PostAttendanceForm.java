package jp.co.internouslab.collegeabsenteesystem.form;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class PostAttendanceForm {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@NotEmpty(message="")
	private String collegeStudentId;

	private String attendanceDate = sdf.format(new Date());

	private Boolean isAbsence = false;

	private Boolean isContact = null;

}
