package jp.co.internouslab.collegeabsenteesystem.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class ChimeForm {

	@NotEmpty
	private int id;

	@NotEmpty
	private boolean stopChimeFlg;

}
