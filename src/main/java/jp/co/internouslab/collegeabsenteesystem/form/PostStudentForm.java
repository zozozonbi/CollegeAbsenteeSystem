package jp.co.internouslab.collegeabsenteesystem.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class PostStudentForm {

	@NotEmpty(message="")
	private String familyName;

	@NotEmpty(message="")
	private String firstName;

	@NotEmpty(message="")
	private String familyNameKana;

	@NotEmpty(message="")
	private String firstNameKana;

	private Integer age;

	private String enterpriseCompany;

	private String tel;

	private String email;

	private Integer viaId;

	private Integer personality;

	private String personalityDetail;

	private String gitHubAccount;

	private String teamGitHubAccount;

	private Integer personInChargeId;

	private String startYear;

	private String startMonth;

	private int isEndCollege;

	private Boolean isNightCollege = false;

}
