package jp.co.internouslab.collegeabsenteesystem.form;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class PutStudentForm {

	@NotNull(message="")
	private int id;

	@NotNull(message="")
	private String familyName;

	@NotNull(message="")
	private String firstName;

	@NotNull(message="")
	private String familyNameKana;

	@NotNull(message="")
	private String firstNameKana;

	private Integer age = null;

	@NotNull(message="")
	private String enterpriseCompany;

	@NotNull(message="")
	private String tel;

	@NotNull(message="")
	private String email;

	private Integer viaId = null;

	private Integer personality = null;

	@NotNull(message="")
	private String personalityDetail;

	@NotNull(message="")
	private String gitHubAccount;

	@NotNull(message="")
	private String teamGitHubAccount;

	private Integer personInChargeId = null;

	@NotNull(message="")
	private String startYear;

	@NotNull(message="")
	private String startMonth;

	private int isEndCollege;

	@NotNull(message="")
	private boolean isNightCollege;

	@NotNull(message="")
	private Timestamp registDate;

}
